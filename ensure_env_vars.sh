#!/bin/bash

ENV_TEMPLATE=./install_env_vars.template
# Get every non commented variable from the template file
DEFAULT_ENVS=$(egrep '[a-zA-Z0-9"'\''\[\]]*=' $ENV_TEMPLATE |egrep -v '^#')

i=0
for env in $DEFAULT_ENVS; do
  IFS== read -r variables[i] default_values[i] <<< "$env"
  ((i++))
done

echo
echo You have not yet specified any default values for the installer!
echo "(Press enter to simply use the default value)"
echo

TMP_ENV_FILE=$(mktemp)

# Ask the user to either confirm the default value or set a new one
for ((i=0;i < ${#variables[@]}; i++)); do
  new_value=""
  while [ -z ${new_value} ]; do
    read -e -p "$(tput bold)${variables[i]}$(tput sgr0): " -i "${default_values[i]}" new_value
    # Erase the previous line
    tput cuu1;tput el
    echo ${variables[i]}=$new_value >> $TMP_ENV_FILE
  done
done

cp $TMP_ENV_FILE files/updates/root/chroot_files/install_env_vars
