#/bin/bash

VERSION=7.20

wget https://www.memtest.org/download/v$VERSION/mt86plus_$VERSION.binaries.zip
unzip -j mt86plus_$VERSION.binaries.zip memtest64.efi -d http/
