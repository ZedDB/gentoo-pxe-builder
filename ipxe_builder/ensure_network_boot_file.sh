#!/bin/bash

echo_info() {
  # Print in bold
  echo -e "\e[1m$@\e[00m"
}

if [ -f network_boot.ipxe ]; then
  # The none templated file exists. Do nothing
  exit 0
fi

get_url() {
  echo_info "Input the final IP/url to the network boot menu.ipxe file."
  echo_info "If you will not use network boot, just press enter."
  echo_info "Don't forget to specify the protocol IE:"
  echo_info "http://my.server/ipxe_folder/menu.ipxe"
  read -p "URL: " boot_url
  echo
}

echo_info No '"network_boot.ipxe"' file found, lets generate it!
get_url

while true; do
  echo_info "This is the final url:"
  echo $boot_url/menu.ipxe
  read -p "Do you want to proceed? (y/n) " yn

  case $yn in
  	[yY] ) break;;
  	[nN] ) echo exiting...;
  		exit 1;;
  	* ) echo invalid response;;
  esac
done

sed "s|IPXE_URL|$boot_url|" network_boot.ipxe.template > network_boot.ipxe
