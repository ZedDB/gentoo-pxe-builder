#!/bin/bash

die(){ echo "$@" 1>&2; missing_command=true; }

command -v gdisk >/dev/null 2>&1 || die "This requires the program 'gdisk' to be installed, install the sys-apps/gptfdisk package."
command -v mkfs.vfat >/dev/null 2>&1 || die "This requires the program 'mkfs.vfat' to be installed, install the sys-fs/dosfstools package."

if [[ ${missing_command} ]]; then
  exit 1
fi

while getopts 'l' OPTION; do
  case "$OPTION" in
    l)
      echo Creating local boot usb drives!
      echo
      use_local_boot="yes"
      ;;
    ?)
      echo "script usage: $(basename \$0) [-s]" >&2
      echo
      echo This script will format usb drives and set the up with bootable iPXE EFI images.
      echo You can edit local_boot.ipxe or network_boot.ipxe to configure which iPXE shell
      echo commands will be automatically be executed when boot from the usb sticks.
      echo
      echo NOTE: This script requires root permissions to format the USB sticks!
      echo
      echo Options:
      echo -e ' '-l'\t' Instead of trying to boot over network, but all boot files on the stick and boot from that.
      exit 1
      ;;
  esac
done
shift "$(($OPTIND -1))"

# Stop execution if any command fails
set -e

if [[ ! ${use_local_boot} ]]; then
  ./ensure_network_boot_file.sh
fi

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

# Use "set -x" for debugging
#set -x

# Create the efi boot image:
cd ipxe/src

if [ -! -f bin-x86_64-efi/ipxe.efi ]; then
  make bin-x86_64-efi/ipxe.efi
fi

# Make sure that the arrays breaks as newline and not any space character
IFS=$'\n'
options=( $(lsblk -I 8,259 -nd --output NAME,SIZE,RM,MODEL | awk '$3==1') )

if [ ${#options[@]} -lt 1 ]; then
  echo No USB drives detected! Exiting the installer...
  exit 1
fi

menu() {
  echo "Avaliable options:"
  printf "\n"

  for i in ${!options[@]}; do
    printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${options[i]}"
  done

  printf "\n"
  if [[ "$msg" ]]; then
    echo "$msg";
  fi
}

prompt="Select drives to install the IPXE boot image on (press ENTER when done): "
while menu && read -rp "$prompt" num && [[ "$num" ]]; do

  # Check if "num" is a valid number.
  [[ "$num" != *[![:digit:]]* ]] &&
  (( num > 0 && num <= ${#options[@]} )) ||
    { msg="Invalid option: $num"; continue; }

  # Decrement the number so it is zero indexed
  ((num--))

  msg="$(echo ${options[num]} | cut -d' ' -f1) was ${choices[num]:+un}checked for formatting"

  [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
done

echo Formatting IPXE image onto device:
for i in ${!options[@]}; do
  if [[ "${choices[i]}" == "+" ]]; then
    STORE_DEV=/dev/$(echo ${options[i]} | cut -d' ' -f1)

    echo ${STORE_DEV}
# format the usb stick:
echo "
o
y
n



ef00
w
y
" | gdisk ${STORE_DEV}

    # Make format the partiton as fat32
    mkfs.vfat ${STORE_DEV}1
    # Mount the drive

    if [ ! -d /tmp/efidrive ]; then
      mkdir /tmp/efidrive
    fi

    mount ${STORE_DEV}1 /tmp/efidrive

    mkdir -p /tmp/efidrive/efi/boot

    cp bin-x86_64-efi/ipxe.efi /tmp/efidrive/efi/boot/bootx64.efi

    if [[ ${use_local_boot} ]]; then
      echo Copying over local boot files to drive...
      cp ../../../http/* /tmp/efidrive/
      cp ../../local_boot.ipxe /tmp/efidrive/efi/boot/autoexec.ipxe
    else
      cp ../../network_boot.ipxe /tmp/efidrive/efi/boot/autoexec.ipxe
    fi

    umount /tmp/efidrive
  fi
done

echo
echo DONE! All usb drives were created successfully!
