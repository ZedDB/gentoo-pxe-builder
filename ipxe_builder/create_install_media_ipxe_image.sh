#!/bin/bash

# Stop execution if any command fails
set -e

if [ -z "$(ls -A ./ipxe)" ]; then
  echo The ipxe source folder is empty, did you forget to checkout submodules?
  echo Alternately, you can manually download the ipxe source code and extract it in that folder.
  exit 1
fi

./ensure_network_boot_file.sh

# Create the efi boot image:
cd ipxe/src

make bin-x86_64-efi/ipxe.efi

cp bin-x86_64-efi/ipxe.efi ../../../files/updates/root/chroot_files/
cp ../../network_boot.ipxe ../../../files/updates/root/chroot_files/autoexec.ipxe
