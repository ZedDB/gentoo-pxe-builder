ipxe_builder
============

This is a set of helper scripts to create bootable iPXE efi images.
These are will automatially try connect to the http server listed in the `network_boot.ipxe` file and continue the boot process from there. 

## create_usb_boot_disk.sh

This script will give you a list of connected USB drives and will format and install the the iPXE efi image to the selected drives.

If the script run with the `-l` flag, it will create a usb stick that will not boot over the network.
The files will be put on the usb drive itself instead and the use the `local_boot.ipxe` file instead of `network_boot.ipxe`.

## create_install_media_ipxe_image.sh

This script will populate the `ipxe.efi` file in the `files` directory at the root of this repository.
The `ipxe.efi` file is used to by the generated Gentoo install media to install bootable iPXE images onto the computers during the install.

This allows the computers to boot with iPXE on their own without need for any usb sticks.

## ipxe folder

This a git submodule clone of the official iPXE repo: https://github.com/ipxe/ipxe

## Shrink usb image

extract usb image:
```
dd if=/dev/sdc of=/tmp/usb_image.iso bs=4M
```

Get the partion end point with `fdisk -l usb_image.iso`

Shrink it with:

33 sectors in needed at GPT stores a table at the end of the disk as well.

```
dd bs=512 count=<End_of_last_partition+1+33> if=usb_image.iso of=usb_shrink.iso
```

Repair the image with gdisk:
```
gdisk usb_shrink.iso
press "v" to verfity that there are errors
press "x" to enter the expert menu
press "e" to move the backup partion table to the new end of the disk
press "w" to write the changes
```

Double check with `fdisk -l usb_shrink.iso` that everything looks OK
