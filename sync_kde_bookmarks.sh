#!/bin/bash

SKEL_DIR="files/updates/root/chroot_files/skel"

mkdir -p $SKEL_DIR/.local/share/

echo '<?xml version="1.0" encoding="UTF-8"?>
<xbel>
 <info>
  <metadata owner="http://www.kde.org">
   <kde_places_version>4</kde_places_version>
   <GroupState-Places-IsHidden>false</GroupState-Places-IsHidden>
   <GroupState-Remote-IsHidden>true</GroupState-Remote-IsHidden>
   <GroupState-Devices-IsHidden>true</GroupState-Devices-IsHidden>
   <GroupState-RemovableDevices-IsHidden>false</GroupState-RemovableDevices-IsHidden>
   <GroupState-Tags-IsHidden>false</GroupState-Tags-IsHidden>
   <withBaloo>true</withBaloo>
   <GroupState-SearchFor-IsHidden>false</GroupState-SearchFor-IsHidden>
   <GroupState-RecentlySaved-IsHidden>false</GroupState-RecentlySaved-IsHidden>
  </metadata>
 </info>
' > $SKEL_DIR/.local/share/user-places.xbel

while read line; do
  mount_point=$(echo $line | awk '{ print $1 }')
  num_col=$(echo $line | awk '{print NF}')
  if [ $num_col -gt 1 ]; then
    name=$(echo $line | cut -d' ' -f 2-)
  else
    name=$(basename $(echo "$line" | sed -e 's/file\:\/\///g'))
  fi
  printf '<bookmark href="%s"> <title>%s</title> </bookmark>\n' "$mount_point" "$name" >> $SKEL_DIR/.local/share/user-places.xbel
done < $SKEL_DIR/.config/gtk-3.0/bookmarks

echo "</xbel>" >> $SKEL_DIR/.local/share/user-places.xbel
