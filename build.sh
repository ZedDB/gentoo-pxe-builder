#!/bin/bash

# defaults
ARCH=${ARCH:-amd64}
MIRROR=${MIRROR:-http://distfiles.gentoo.org}
SSH_KEY_PATH=${SSH_KEY_PATH:-~/.ssh/id_ed25519.pub}

die(){ echo "$@" 1>&2; missing_command=true; }
pushd_q(){ pushd "$@" > /dev/null; }
popd_q(){ popd "$@" > /dev/null; }

command -v cpio >/dev/null 2>&1 || die "This requires the program 'cpio' to be installed"
command -v gpg >/dev/null 2>&1 || die "This requires the program 'gpg' to be installed"
command -v xz >/dev/null 2>&1 || die "This requires the program 'xz' to be installed"
command -v isoinfo >/dev/null 2>&1 || die "This requires the program 'isoinfo' (from cdrtools) to be installed"

if [[ ${missing_command} ]]; then
  exit 1
fi

# Stop execution if any command fails
set -e

UPDATE_DIR=files/updates
CHR_FILES=$UPDATE_DIR/root/chroot_files

# Ensure that all config files we need to create the installer is present
if [ ! -f $CHR_FILES/ipxe.efi ]; then
  pushd_q ipxe_builder
  ./create_install_media_ipxe_image.sh
  popd_q
fi

if [ ! -f $CHR_FILES/install_env_vars ]; then
  ./ensure_env_vars.sh
fi

gpg --locate-key releng@gentoo.org

mkdir -p iso
pushd_q iso
	base_url="${MIRROR}/releases/${ARCH}/autobuilds"

	latest_iso=$(curl "${base_url}/latest-iso.txt" 2>/dev/null | grep -e '-minimal-' | cut -d " " -f 1 | head -1)
	iso=$(basename "${latest_iso}")

	wget -nc "${base_url}/${latest_iso}" || die "Could not download iso"
	wget -nc "${base_url}/${latest_iso}.DIGESTS" || die "Could not download digests"
	wget -nc "${base_url}/${latest_iso}.CONTENTS.gz" || die "Could not download contents"
	sha512_digests=$(grep -A1 SHA512 "${iso}.DIGESTS" | grep -v '^--')
	gpg --verify "${iso}.DIGESTS" || die "Insecure digests"
	echo "${sha512_digests}" | sha512sum -c || die "Checksum validation failed"
popd_q
#iso=install-amd64-minimal-20240513T191852Z.iso

iso=iso/${iso}
isoinfo -R -i ${iso} -X -find -path /boot/gentoo && mv -vf boot/gentoo .
isoinfo -R -i ${iso} -X -find -path /image.squashfs
isoinfo -R -i ${iso} -X -find -path /boot/gentoo.igz
mv image.squashfs squashfs.img
(cat boot/gentoo.igz; (echo squashfs.img | cpio -H newc -o)) > gentoo.igz
rm -f squashfs.img
rm -f boot/gentoo.igz
rmdir boot

[ -f "${SSH_KEY_PATH}" ] && mkdir -p $UPDATE_DIR/root/.ssh && cp "${SSH_KEY_PATH}" $UPDATE_DIR/root/.ssh/authorized_keys

# Sync/convert the gnome bookmarks to KDE bookmarks
./sync_kde_bookmarks.sh

# dracut will copy the folder structure in "/updates" to the live root file system
pushd_q files
	find . -print | cpio -H newc -o | xz --check=crc32 -vT0 >> ../gentoo.igz
	[ -f updates/root/.ssh/authorized_keys ] && rm updates/root/.ssh/authorized_keys
popd_q

# Clean up kde bookmarks file
rm $CHR_FILES/skel/.local/share/user-places.xbel

# Move the files into place for easy testing
mv -f gentoo gentoo.igz http/

echo "All done:"
echo "---------"
echo "  - PXE kernel file : http/gentoo"
echo "  - PXE initramfs file : http/gentoo.igz"
