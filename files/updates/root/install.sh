#!/bin/bash
# Based on https://github.com/NiKiZe/Gentoo-HAI/blob/master/install.sh

# Look at https://wiki.gentoo.org/wiki/Handbook:AMD64/Installation for instructions on how to install Gentoo

is_server=$(cat /proc/cmdline | grep server)

echo_mess() {
  # Print in green and bold
  echo
  echo -e "\e[1;32m$@\e[00m"
  echo
}

echo_info() {
  # Print in bold
  echo -e "\e[1m$@\e[00m"
}

echo_warn() {
  # Print in inverted yellow
  echo
  echo -e "\e[33;7m$@\e[0m"
  echo
}

echo_err() {
  # Print in red and bold
  echo
  echo -e "\e[1;31m$@\e[00m"
  echo
}

echo_info NOTE: Some motherboards do not report WOL state correctly. Make sure that "'power on by PCI-E'" is turned on in the UEFI settings.

#Check if Wake on Lan is activated on the computer. Warn if it is not.
for iface in /sys/class/net/*
do
  if [ -f "$iface/device/uevent" ]; then
    echo -n " ${iface##*/} WOL: "
    # This is a non virtual device, check if wake on lan is enabled.
    wol=$(ethtool ${iface##*/} | grep "Supports Wake-on" | grep g)
    if [ -z "$wol" ]; then
      # No Wake on Lan (Red, bold)
      echo -en "\e[1;31mNO\e[00m"
    else
      # Wake on Lan is turned on (Green, bold)
      echo -en "\e[1;32mYES\e[00m"
    fi
  fi
done

# Stop execution if any command fails
set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

# Use "set -x" for debugging
#set -x

#IF NOT SET_PASS is set then the password will be "password"
SET_PASS=${SET_PASS:-password}

# Try to update to a correct system time
chronyd -q > /dev/null 2>&1 &
pid_ntp=$!

# Make sure our root mountpoint exists
mkdir -p /mnt/gentoo

echo_warn === This installer will WIPE the computer. Press ctrl+c to exit. ===

# The magic filtering numbers for "lsblk -I" represents:
# 8: SATA (/dev/sdX) devices
# 252: Virtual (/dev/vdX) devices
# 259: Nvme (/dev/nvmeXnY) devices
#
# Make sure that the arrays breaks at newline and not any space character
IFS=$'\n'
options=( $(lsblk -I 8,252,259 -nd --output NAME,SIZE,MODEL) )

if [ ${#options[@]} -lt 1 ]; then
  echo_err No hard drives detected! Exiting the installer...
  exit 1
fi

if ! [ -d /sys/firmware/efi ]; then
  echo_err Only EFI platforms are supported! Exiting the installer...
  exit 1
fi

echo_info Input the desired hostname of the computer.
# While we technically can accept more advanced hostnames it will
# most likely mess up avahi/mDNS stuff, so enforce simple hostnames
echo "(Only alphanumeric characters and hyphens are allowed)"
while IFS= read -p "Hostname: " install_hostname; do
  echo "$install_hostname" | grep -qP '^(?![0-9]+$)(?!.*-$)(?!-)[a-zA-Z0-9-]{1,63}$' && break
  if [[ -z "$install_hostname" ]]; then
    echo_info Hostname can not be empty
    continue
  fi
  # Can't start or end with hyphens
  if [[ "$install_hostname" =~ ^(.*-$|-.*)$ ]]; then
    echo_info Hostname can not start or end with a hyphen!
    continue
  fi
  # Can't only be a number
  if [[ "$install_hostname" != *[![:digit:]]* ]]; then
    echo_info Hostname can not be a number!
    continue
  fi
  # Can not be longer than 63 characters
  if [[ ${#install_hostname} -gt 63 ]]; then
    echo -en "\e[1mHostname can not be longer than 63 characters:\e[00m"
    # Point to the character that went over the limit
    printf '%27s%b\n' "" "\e[33;7m↑\e[0m"
    continue
  fi
  offset=10 #10 for the intial "Hostname: " length
  for (( i=0; i<${#install_hostname}; i++ )); do
    if echo "${install_hostname:$i:1}" | grep -qP [a-zA-Z0-9-]; then
      continue
    else
      printf "%*s%b" $((offset+i)) "" "\e[33;7m↑\e[0m"
      offset=$((-i-1))
    fi
  done
  echo
  echo_info The host name contains invalid characters!
done

menu() {
  echo "Avaliable options:"
  printf "\n"

  for i in ${!options[@]}; do
    symbol="${choices[i]:- }"
    [[ "$symbol" == "r" ]] && symbol="\e[31m$symbol\e[00m"
    printf "%3d%b) %s\n" $((i+1)) $symbol "${options[i]}"
  done

  printf "\n"
  if [[ "$msg" ]]; then
    echo_info "$msg";
  fi
}

root_drive=-1
mode="root"

prompt="Check root drive ('s' to switch to storage device mode, ENTER when done): "
while menu && read -rp "$prompt" num && ( [[ "$num" ]] || [[ root_drive -lt 0 ]] ); do

  # If num is empty then it means that we have not selected a valid root drive yet.
  [[ -z "$num" ]] &&
    { msg="A root drive needs to be selected!"; continue; }

  if [[ "$num" == "s" ]]; then
    if [[ "$mode" == "root" ]]; then
      msg="Switched to storage select mode."
      mode="storage"
      prompt="Check storage drive(s) (again to uncheck, ENTER when done): "
    else
      msg="Switched to root select mode."
      mode="root"
      prompt="Check root drive ('s' to switch to storage mode, ENTER when done): "
    fi
    continue
  fi

  # Check if "num" is a valid number.
  [[ "$num" != *[![:digit:]]* ]] &&
  (( num > 0 && num <= ${#options[@]} )) ||
    { msg="Invalid option: $num"; continue; }

  # Decrement the number so it is zero indexed
  ((num--))

  if [[ "$mode" == "root" ]]; then
    msg="Selected $(echo ${options[num]} | cut -d' ' -f1) as root drive"

    if [[ root_drive -gt -1 ]]; then
      # Reset the old choice for root drive
      choices[root_drive]=""
    fi
    choices[num]="r"
    root_drive=$num
  else
    msg="$(echo ${options[num]} | cut -d' ' -f1) was ${choices[num]:+un}checked as storage device"

    [[ num -eq root_drive ]] && { root_drive=-1; choices[num]=""; }
    [[ "${choices[num]}" ]] && choices[num]="" || choices[num]="+"
    echo $root_drive
  fi
done

# wait to make sure ntpd is done
echo_mess Waiting for ntpd...
echo If this takes longer than a minute, then it might be that you were not able to establish an internet connection
wait $pid_ntp

# No interactivity from this point on, start timer to see how long the install took.
SECONDS=0 # Bash magic variable, bash will automatically increment this varaible each second.

num_storage_devs=0

echo_mess Formatting storage device:
for i in ${!options[@]}; do
  if [[ "${choices[i]}" == "r" ]]; then
    # Define root device variables but don't format yet
    ROOT_DEV=/dev/$(echo ${options[i]} | cut -d' ' -f1)
    ROOT_DEVP=${ROOT_DEV}
    # If disk name ends with number, then partition is sepparated with p
    echo ${ROOT_DEV} | grep -q -e "[0-9]$" && ROOT_DEVP=${ROOT_DEV}p

    continue
  fi

  if [[ "${choices[i]}" == "+" ]]; then
    # ++ first because otherwise bash thinks this is an error because it returns zero
    ((++num_storage_devs))
    STORE_DEV=/dev/$(echo ${options[i]} | cut -d' ' -f1)
    STORE_DEVP=${STORE_DEV}
    # If disk name ends with number, then partition is sepparated with p
    echo ${STORE_DEV} | grep -q -e "[0-9]$" && STORE_DEVP=${STORE_DEV}p

    echo -e '\t'$STORE_DEVP

    #Note that the code below is not indented as it would mess up the commands passed to gdisk
echo "
o
y
n




c
storage$num_storage_devs
w
y
" | gdisk ${STORE_DEV}

    mkfs.ext4 -F ${STORE_DEVP}1
  fi
done

echo_mess Formatting root drive...
      echo -e '\t'$ROOT_DEVP

#Create a 256MB boot partition and the rest as a root partition on ${ROOT_DEV}
echo "
o
y
n


+256M
ef00
c
boot
n




c
2
rootfs
w
y
" | gdisk ${ROOT_DEV}

mkfs.vfat ${ROOT_DEVP}1
mkfs.ext4 -F ${ROOT_DEVP}2

mount ${ROOT_DEVP}2 /mnt/gentoo -o discard,noatime
mkdir -p /mnt/gentoo/boot
mount ${ROOT_DEVP}1 /mnt/gentoo/boot

echo_mess "Trying to grab Gentoo releng & infrastructure gpg key..."
gpg --locate-key releng@gentoo.org; gpg --locate-key infrastructure@gentoo.org

# cd into our future file system root directory.
cd /mnt/gentoo

echo_mess Trying to fetch the latest gentoo distribution files...

if [[ "$is_server" ]]
then
  # Server
  # Download the stage3 file from the official mirror
  DISTMIRROR=http://distfiles.gentoo.org
  DISTBASE=${DISTMIRROR}/releases/amd64/autobuilds/current-stage3-amd64-desktop-openrc/
else
  # Client
  # Download the stage3 file from the local build server
  source ~/chroot_files/install_env_vars
  DISTBASE=http://${SERVER_ADDR}/stage3/
fi

FILE=$(wget -q "${DISTBASE}latest-stage3-amd64-desktop-openrc.txt" -O - | grep -e '-desktop-' | cut -d " " -f 1 | head -1)
[ -z "$FILE" ] && echo_err No stage3 found on $DISTBASE. Exiting the installer... && exit 1
echo_info Downloading latest stage file $FILE
wget $DISTBASE$FILE
wget $DISTBASE$FILE.DIGESTS

gpg --verify $FILE.DIGESTS
echo_info "Verifying stage3 SHA512 ..."
# grab SHA512 lines and line after, then filter out line that ends with iso
echo "$(grep -A1 SHA512 $FILE.DIGESTS | grep $FILE\$)" | sha512sum -c || bash
echo_mess " - Awesome! stage3 verification looks good."
echo_info "Extracting stage3..."
tar xpf $FILE --xattrs-include='*.*' --numeric-owner --checkpoint=.1000

if [[ $is_server ]]
then
  # Copy over the stage3 files so we can rehost it for the client computers.
  mkdir var/cache/stage3
  cp $FILE var/cache/stage3/
  cp $FILE.DIGESTS var/cache/stage3/
  wget -P var/cache/stage3 ${DISTBASE}latest-stage3-amd64-desktop-openrc.txt
fi

wait
rm $FILE
rm $FILE.DIGESTS

echo_mess Configuring fstab...

# Change fstab to match disk layout
echo -e "
PARTLABEL=boot		/boot		vfat		noauto,noatime	1 2
PARTLABEL=rootfs		/		ext4		noatime	0 1
" >> etc/fstab

for i in $(seq 1 $num_storage_devs); do
  echo -e "PARTLABEL=storage$i		/mnt/storage$i		ext4		noatime	0 1" >> etc/fstab
  mkdir mnt/storage$i
  mount PARTLABEL=storage$i mnt/storage$i
  chmod 777 mnt/storage$i
done

# Remove template entries in fstab
sed -i '/\/dev\/BOOT.*/d' etc/fstab
sed -i '/\/dev\/ROOT.*/d' etc/fstab
sed -i '/\/dev\/SWAP.*/d' etc/fstab

echo_mess Setting up make.conf...

MAKECONF=etc/portage/make.conf
[ ! -f $MAKECONF ] && [ -f etc/make.conf ] && MAKECONF=etc/make.conf
echo $MAKECONF

#Updating Makefile
sed -i 's/COMMON_FLAGS="-O2 -pipe"/COMMON_FLAGS="-march=x86-64-v3 -O2 -pipe -fdiagnostics-color=always"/' $MAKECONF

echo >> $MAKECONF
echo 'MAKEOPTS="-j'$(nproc)'"' >> $MAKECONF
echo 'CPU_FLAGS_X86="aes avx avx2 f16c fma3 mmx mmxext pclmul popcnt sse sse2 sse3 sse4_1 sse4_2 ssse3"' >> $MAKECONF
echo >> $MAKECONF
echo 'USE="elogind -consolekit -systemd X wayland xwayland gles2 vaapi vulkan libglvnd pipewire pulseaudio screencast zeroconf"' >> $MAKECONF
echo 'VIDEO_CARDS="amdgpu radeon radeonsi intel i965 iris"' >> $MAKECONF
echo 'INPUT_DEVICES="libinput wacom"' >> $MAKECONF
echo >> $MAKECONF
echo 'ACCEPT_LICENSE="* -@EULA"' >> $MAKECONF
echo 'BINPKG_FORMAT="gpkg"' >> $MAKECONF

echo_mess Setting up rc.conf...

echo 'rc_parallel="YES"' >> etc/rc.conf
echo 'rc_interactive="YES"' >> etc/rc.conf

#echo Setting up default dhcp for the network cards...
#
#for net_dev in /sys/class/net/e*
#do
#  net_name=$(basename $net_dev)
#  echo "config_${net_name}=dhcp" >> etc/conf.d/net
#done

echo_mess Copying over portage package settings

# We do not use the update_portage_conf.sh script at this point because we do not have git installed.
# For ease of use, we also want the portage config installed as soon as possible.
wget https://projects.blender.org/ZedDB/studio_portage_conf/archive/master.tar.gz
tar -xf master.tar.gz --directory=/tmp/
cp -r /tmp/studio_portage_conf/* etc/portage/
rm master.tar.gz

echo_mess Copying over /etc/skel configurations

cp -r ~/chroot_files/skel/. etc/skel

# Append some extra options to the bashrc defaults
cat ~/chroot_files/skel_bashrc_extras >> etc/skel/.bashrc
# Make sure root is using the same .bashrc as well
cp etc/skel/.bashrc root/
cp etc/skel/.bash_profile root/

echo_mess Copying over misc files

# Copy over the custom en_NL locale file for glibc
# (English with dutch date format and conventions)
cp ~/chroot_files/en_NL usr/share/i18n/locales/

if [[ ! $is_server ]]
then
  # Client
  # Save the IP adress or DNS name for the server.
  # This will be used by the studio scripts to communicate between the client and the server.
  mkdir etc/studio_scripts
  echo $SERVER_ADDR > etc/studio_scripts/server_addr
fi

echo_info Preparing to chroot...
# Setup hostname
echo "# Set to the hostname of this machine
hostname=\"${install_hostname}\"
" > etc/conf.d/hostname

# Setup chroot mount points
mount --types proc /proc proc
mount --rbind /sys sys
mount --rbind /dev dev
mount --bind /run run

cp /etc/resolv.conf etc

# Make sure that we can access files from the install medium that we use with the chroot script
mkdir tmp/chroot_files
mount --bind ~/chroot_files tmp/chroot_files

echo_info Base install is in place, now we will chroot and continue the setup from there...

chroot . tmp/chroot_files/chrootstart.sh

# Store total execution time
tot_sec=$SECONDS
sec=$((tot_sec%60))
min=$((tot_sec/60%60))
hours=$((tot_sec/60/60))

echo_info Total elapsed time: $hours h, $min m, $sec s

echo_warn === Installation done! Hit any key to reboot. ===

read -n1 -s -r

reboot
