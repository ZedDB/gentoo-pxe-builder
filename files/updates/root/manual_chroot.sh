#!/bin/bash

mkdir -p /mnt/gentoo

# Stop execution if any command fails
set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

# Make sure that the arrays breaks as newline and not any space character
IFS=$'\n'
options=( $(lsblk -I 8,259 -nrp --output NAME,SIZE,TYPE,PARTLABEL,MOUNTPOINTS | awk '$3 == "part"' | awk '!($3="")' | column -t) )

echo "         Select the partition to chroot into:"
echo

COLUMNS=1
select drive_info in "${options[@]}"
do
  if [[ $REPLY == *[![:digit:]]* ]] || [ $REPLY -lt 1 ] || [ ${#options[@]} -lt $REPLY ]; then
    echo Input a valid number please!
    continue
  fi
  break
done

echo $drive_info

mount_point=$(echo $drive_info | awk '{print $4}')

if [ -z "$mount_point" ]; then
  drive=$(echo $drive_info | awk '{print $1}')
  mount $drive /mnt/gentoo -o discard,noatime
  mount_point=/mnt/gentoo
fi

cd $mount_point

# Setup chroot mount points
mount --types proc /proc proc
mount --rbind /sys sys
mount --rbind /dev dev
mount --bind /run run

cp /etc/resolv.conf etc

# Make sure that we can access files from the install medium that we use with the chroot script
mkdir -p tmp/chroot_files
mount --bind ~/chroot_files tmp/chroot_files

# Use "-l" here to make sure that we source /etc/profile when chrooting
chroot . /bin/bash -l

# Make sure that we don't start un-mounting mount points inside of /sys and /dev
mount --make-rslave sys
mount --make-rslave dev

# Unmount the chroot directory
cd ..
umount -R gentoo
