# Don't start the install script promt when not on tty1
if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  # Make sure that authorized_keys has the correct permissions and owner
  # (Otherwise it will be ignored when trying to ssh into the machine)
  [ -f ".ssh/authorized_keys" ] && chmod 600 .ssh/authorized_keys && chown root:root .ssh/authorized_keys
  reset
  ./install.sh
fi
