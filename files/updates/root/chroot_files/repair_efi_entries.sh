#!/bin/bash

set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

mount /boot

grub-install --target=x86_64-efi --efi-directory=/boot

# Add the iPXE payload to the EFI boot options
efibootmgr -c -d $(findmnt -o SOURCE -n /boot) -L "iPXE" -l '\EFI\ipxe\ipxe.efi'
# Explicitly set the boot order
gentoo_boot_nr=$(efibootmgr | grep gentoo | awk '{print $1}' | sed 's/Boot//' | sed 's/\*//')
ipxe_boot_nr=$(efibootmgr | grep iPXE | awk '{print $1}' | sed 's/Boot//' | sed 's/\*//')

efibootmgr -o $gentoo_boot_nr,$ipxe_boot_nr

echo Repair done!
