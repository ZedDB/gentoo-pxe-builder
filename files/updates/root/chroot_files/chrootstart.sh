#!/bin/bash

is_server=$(cat /proc/cmdline | grep server)

# Stop execution if any command fails
set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

source /etc/profile
source /tmp/chroot_files/install_env_vars

export PS1="(chroot) ${PS1}"

if [[ $is_server ]]
then
  # Server
  echo "Syncing server portage"
  # Download the latest snapshot of portage
  emerge-webrsync

  # Clean up websync temp files and update to the most recent portage state
  emerge --sync

  # Setup local rsync mirror for the client computers
  line_start=$(cat /etc/rsyncd.conf | grep -n "\[gentoo-portage\]" | cut -d : -f 1)
  sed -i $line_start',$ s/^##*//' /etc/rsyncd.conf # Uncomment the gentoo rsync repo lines

  rc-update add rsyncd default

  # Build binary packages for every package we build
  echo 'FEATURES="buildpkg parallel-install"' >> /etc/portage/make.conf
  # Except for these packages:
  echo EMERGE_DEFAULT_OPTS=\""--buildpkg-exclude 'virtual/* sys-kernel/*-sources'\"" >> /etc/portage/make.conf

  # Update outdated packages
  emerge -Du --newuse world

  # Re-emerge all packages to we have binary packages available for them (and so the cflags are up to date)
  emerge --emptytree --usepkg=n world
else
  # Client
  echo "Syncing client portage"

  mkdir /etc/portage/repos.conf
  cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo-mirror.conf
  sed -i 's/sync-uri = .*/sync-uri = rsync:\/\/'$SERVER_ADDR'\/gentoo-portage/' /etc/portage/repos.conf/gentoo-mirror.conf

  # Configure the clients to try to download precompiled binary packages from the local server.
  echo 'FEATURES="getbinpkg parallel-install -ebuild-locks -news"' >> /etc/portage/make.conf
  echo 'EMERGE_DEFAULT_OPTS="--jobs='$(nproc)'"' >> /etc/portage/make.conf

  # Remove all other bin repo configs
  rm /etc/portage/binrepos.conf/*
  echo '[local-build-server]' > /etc/portage/binrepos.conf/local_build_server.conf
  echo 'sync-uri = http://'$SERVER_ADDR'/packages' >> /etc/portage/binrepos.conf/local_build_server.conf
  echo 'priority = 10' >> /etc/portage/binrepos.conf/local_build_server.conf

  emerge --sync

  # Make sure all the system packages are up to date with the build server
  emerge --verbose --update --deep --newuse --quiet --with-bdeps=y world
fi

# Skip all updates to the etc files (at this point we are quite sure that none of our modifcation to /etc should be overwritten/updated)
echo "-9
YES
" | etc-update

# "Read" all news items from portage
eselect news read new

# Cleanup any old packages
emerge --depclean

# Update timezone
echo "$TIMEZONE" > /etc/timezone
rm /etc/localtime

emerge --config sys-libs/timezone-data

# Set locales available on the system

echo en_NL.UTF-8 UTF-8 >> /etc/locale.gen

locale-gen

eselect locale set en_NL.utf8

env-update && source /etc/profile && export PS1="(chroot) ${PS1}"

# Add non offical repos to the package manager
emerge app-eselect/eselect-repository dev-vcs/git
mkdir -p /etc/portage/repos.conf
eselect repository add blender-studio-overlay git https://projects.blender.org/ZedDB/blender-studio-overlay.git
emaint sync --repo blender-studio-overlay

# Add steam for easy VR gear testing
eselect repository enable steam-overlay
emaint sync --repo steam-overlay

# Make sure that /boot is mounted for grub, the kernel, and iPXE installation
mountpoint -q /boot || mount /boot

# Make sure efibootmgr is installed
emerge -1 sys-boot/efibootmgr

# Make sure that the efi boot table does not contain any "gentoo" and "iPXE" label entries.
# We will add these ourselves. On some motherboards duplicate label are automatically overwritten with the new entry.
# But on others they are not so we have to account for that and make sure the boot table is in a clean state.
for entry in $(efibootmgr | grep gentoo | awk '{print $1}' | sed 's/Boot//' | sed 's/\*//'); do
  # Delete entry
  efibootmgr -B -b $entry
done
for entry in $(efibootmgr | grep iPXE | awk '{print $1}' | sed 's/Boot//' | sed 's/\*//'); do
  # Delete entry
  efibootmgr -B -b $entry
done

efibootmgr --remove-dups

# Helper tools needed to install the kernel and clean up
emerge sys-kernel/installkernel eclean-kernel

# Setup the default gurb config (grub is pulled in by the installkernel package)
echo GRUB_GFXMODE=1920x1080,1024x768x32,auto >> /etc/default/grub
echo GRUB_GFXPAYLOAD_LINUX=keep >> /etc/default/grub
echo GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash video=1920x1080\" >> /etc/default/grub

# Setup and install the kernel
emerge @kernel-set

# Copy over the iPXE boot file so we can use it to boot it without using usb sticks.
mkdir /boot/EFI/ipxe
cp /tmp/chroot_files/ipxe.efi /boot/EFI/ipxe/
cp /tmp/chroot_files/autoexec.ipxe /boot/EFI/ipxe/
# Add it to the EFI boot options
efibootmgr -c -d $(findmnt -o SOURCE -n /boot) -L "iPXE" -l '\EFI\ipxe\ipxe.efi'
# Explicitly set the boot order
gentoo_boot_nr=$(efibootmgr | grep gentoo | awk '{print $1}' | sed 's/Boot//' | sed 's/\*//')
ipxe_boot_nr=$(efibootmgr | grep iPXE | awk '{print $1}' | sed 's/Boot//' | sed 's/\*//')

efibootmgr -o $gentoo_boot_nr,$ipxe_boot_nr

# Add a script to reboot to ipxe
echo "#!/bin/bash" > ~/reboot_to_ipxe.sh
echo "efibootmgr -n $ipxe_boot_nr && reboot" >> ~/reboot_to_ipxe.sh
chmod +x ~/reboot_to_ipxe.sh

# Setup dhcp for the network cards
emerge net-misc/dhcpcd
rc-update add dhcpcd default

# Install logging
emerge metalog
rc-update add metalog default

# Install time synchronization
emerge net-misc/chrony
rc-update add chronyd default

# Install filesystem tools for vfat and nfs
emerge sys-fs/dosfstools net-fs/nfs-utils

# Nice to have portage helper tools
emerge eix gentoolkit smart-live-rebuild

# Populate the eix database
eix-update

# Install a cron daemon
emerge fcron
emerge --config fcron
rc-update add fcron default

# Set root password
echo "root:$ROOT_PASSWORD" | chpasswd

if [[ $is_server ]]
then
  # Server
  # Server specific build flags
  echo "sys-apps/studio_scripts server" >> /etc/portage/package.use/utils
  echo "sys-apps/hwdump extras" >> /etc/portage/package.use/utils
  # Create a reverse use file that we can use to build the client computer packages of these.
  mkdir /etc/studio_scripts
  echo "sys-apps/studio_scripts -server" >> /etc/studio_scripts/client_use
  echo "sys-apps/hwdump -extras" >> /etc/studio_scripts/client_use

  # Ensure that the client versions are available on the build server
  USE="-server" emerge -1B studio_scripts
  USE="-extras" emerge -1B hwdump

  # Setup binary package download  http server
  emerge www-servers/lighttpd

  echo 'server.modules += ( "mod_alias" )' >> /etc/lighttpd/lighttpd.conf
  echo 'alias.url = ( "/packages" => "/var/cache/binpkgs/" )' >> /etc/lighttpd/lighttpd.conf
  echo 'alias.url += ( "/update_info" => "/var/cache/update_info/" )' >> /etc/lighttpd/lighttpd.conf
  mkdir /var/cache/update_info
  echo 'alias.url += ( "/stage3" => "/var/cache/stage3/" )' >> /etc/lighttpd/lighttpd.conf
  echo >> /etc/lighttpd/lighttpd.conf
  echo '$HTTP["url"] =~ "^/stage3($|/)" { dir-listing.activate = "enable" }' >> /etc/lighttpd/lighttpd.conf

  # For some reason the ipv6 support restricts the server to v6 and ignores v4.
  # Disable ipv6 until we figure this out.
  sed -e '/^server.use-ipv6/ s/^#*/#/' -i /etc/lighttpd/lighttpd.conf

  rc-update add lighttpd default

  rc-update add sshd default

  # Setup a NFS server
  #mkdir /shared
  #chmod 777 /shared
  #echo "/shared    *(insecure,rw,sync,no_subtree_check)" >> /etc/exports
  #sed -i 's/OPTS_RPC_NFSD="8"/OPTS_RPC_NFSD="8 -V 3 -V 4"/' /etc/conf.d/nfs
  #rc-update add nfs default

  useradd -m -G users,wheel -s /bin/bash $SERVER_USER
  echo "$SERVER_USER:$SERVER_PASSWORD" | chpasswd
  # Server configuration done!

  # fcron tab entries
cat > /tmp/fcrontab_input << EOF
!erroronlymail(true)
# Check if we should update MOTD every 5 minutes with info about reported errors
@nolog(true) 5 /root/refresh_motd.sh
# Build new blender binary packages every night ASAP between 03:00 and 05:59.
%nightly * 3-5 emerge -1 blender blender-bin flamenco blender-studio-tools
# Trim SSD drivers once per week
%weekly * * /sbin/fstrim --all
EOF
  # Use the fcrontab
  fcrontab /tmp/fcrontab_input
fi

emerge tuigreet display-manager-init

sed -i 's/DISPLAYMANAGER=.*/DISPLAYMANAGER="greetd"/' /etc/conf.d/display-manager
sed -i 's/^command = .*/command = "tuigreet -a -r --remember-user-session --xsession-wrapper '\''dbus-run-session ssh-agent startx'\'' --session-wrapper '\''ssh-agent'\'' --power-shutdown '\''loginctl poweroff'\'' --power-reboot '\''loginctl reboot'\''"/' /etc/greetd/config.toml

if [[ -z $is_server ]]
then
  # Client

  # Add elogind and our login manager to default startup
  rc-update add elogind boot
  rc-update add display-manager default
  # Autostart sshd
  rc-update add sshd default

  # Ensure that the "pipewire" group is available (used instead of the "audio" group)
  # Also ensure that the bluetooth serice is available
  emerge -1 media-video/pipewire

  # Enable bluetooth
  rc-update add bluetooth default

  useradd -m -G users,wheel,pipewire,video,input -s /bin/bash $CLIENT_USER
  echo "$CLIENT_USER:$CLIENT_PASSWORD" | chpasswd

  # Have CUPS available for printing support (add cups-browsed instead if you want have cups auto add printers on the network)
  rc-update add cupsd default
  # Add mdns for CUPS (Avahi) network lookups
  emerge sys-auth/nss-mdns
  # Update /etc/nsswitch.conf
  sed -i 's/hosts:      files dns/hosts:      files mdns4_minimal [NOTFOUND=return] dns mdns4/' /etc/nsswitch.conf

  # Allow user to print
  gpasswd -a $CLIENT_USER lp
  # Allow user to add and change printer settings
  gpasswd -a $CLIENT_USER lpadmin

  # Enable autologin
  echo "allow_autologin = true" >> /etc/greetd/config.toml

  # Use Networkmanager to manage the network connections
  rc-update del dhcpcd default
  emerge networkmanager
  rc-update add NetworkManager default
  gpasswd -a $CLIENT_USER plugdev # Allow users to change the network connections via Networkmanager
  # Ensure network manger respects the hostname we set.
  echo "[main]" > /etc/NetworkManager/NetworkManager.conf
  echo plugins=keyfile >> /etc/NetworkManager/NetworkManager.conf
  echo hostname-mode=none >> /etc/NetworkManager/NetworkManager.conf
  # Ensure Wake-on-Lan is enabled for network cards that needs have the feature manually enabled on every boot
  # See 'man NetworkManager.conf' and 'man nm-settings' for details
  echo "[connection]" >> /etc/NetworkManager/NetworkManager.conf
  echo ethernet.wake-on-lan=0x40 >> /etc/NetworkManager/NetworkManager.conf
  # Make sure that we wait at least 10 seconds for the computer to connect to the network on boot up.
  # Without this, system services that depend on network connections (like autofs, flamenco) will start
  # before they can connect successfully. This leads to them reporting network related errors even if
  # they will start working just fine a few seconds later.
  sed -i 's/INACTIVE_TIMEOUT=1/INACTIVE_TIMEOUT=10/' /etc/conf.d/NetworkManager

  # Install autofs to nicely mount/unmount the network drives depending on if they are avaiable or not
  emerge autofs
  rc-update add autofs default

  sed -i 's/#mount_nfs_default_protocol = 3/mount_nfs_default_protocol = 4.2/' /etc/autofs/autofs.conf
  echo "/- /etc/autofs/auto.shared --ghost" >> /etc/autofs/auto.master
  echo "/shared -rw shared.blender:/data/shared" >> /etc/autofs/auto.shared
  echo "/render -rw render.blender:/data/render" >> /etc/autofs/auto.shared

  # fcron tab entries
  # All of the .sh scripts are from the "studio_scripts" package (in the utils portage set)
cat > /tmp/fcrontab_input << EOF
!erroronlymail(true)
# Run every five minutes to check if we should update the system.
@nolog(true) 5 update_if_date.sh
# Try to update the blender-set once every day between 05:00 to 23:59
%daily,rebootreset(true) * 5-23 run_daily_updates.sh
# Run the hw dump script once every week.
%weekly * * dump_hw_to_server.sh
# Push error email to shared every five mintues if there is any
@nolog(true) 5 dump_errors_to_server.sh
# Trim SSD drivers once per week
%weekly * * /sbin/fstrim --all
EOF

  # Use the fcrontab
  fcrontab /tmp/fcrontab_input

  # Setup the "data" folder.
  if [ -d "/mnt/storage1" ]
  then
    ln -s /mnt/storage1 /data
  else
    mkdir /data
    chmod 777 /data
  fi
fi

# Install administation scripts
emerge @utils
# Setup rsync upload for data submitted by the studio (non server) computers
if [[ $is_server ]]
then
  # Server
  mkdir -p /var/studio_scripts/hw_data
  mkdir -p /var/studio_scripts/errors
  # Ensure that the default rsync user "nobody" can write to the folders
  chown -R nobody /var/studio_scripts

  # Add config for resyncing
echo "
[client-hw-info]
        path = /var/studio_scripts/hw_data/
        comment = Hardware data uploaded by client computers
        read only = no
        write only = yes
        refuse options = delete
[client-errors]
        path = /var/studio_scripts/errors/
        comment = Errors uploaded by the clients
        read only = no
        write only = yes
        refuse options = delete
" >> /etc/rsyncd.conf
fi

# Install the package sets on the server as well to provide binary packages for the clients
emerge @workstation-set

# Add default flatpak repo
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
