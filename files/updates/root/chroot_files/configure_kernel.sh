#!/bin/bash

set -e

function Error()
{
  echo "$0: Error occured at line $1"
}
trap 'Error $LINENO' ERR

# TODO preemt differently for the server

# List of non standard kernel parameters that we want.
echo "
# Enable low latency desktop mode
CONFIG_PREEMPT=y

# NVME support
CONFIG_NVME_COMMON=y
CONFIG_NVME_CORE=y
CONFIG_BLK_DEV_NVME=y
CONFIG_NVME_MULTIPATH=y
CONFIG_NVME_VERBOSE_ERRORS=y
CONFIG_NVME_HWMON=y
CONFIG_NVME_FABRICS=m
CONFIG_NVME_FC=m
CONFIG_NVME_TCP=m
CONFIG_NVME_AUTH=y

# Misc file system support
CONFIG_BLK_MQ_VIRTIO=y
CONFIG_VIRTIO=y
CONFIG_FUSE_FS=y
CONFIG_VIRTIO_FS=y

CONFIG_EXFAT_FS=y
CONFIG_EXFAT_DEFAULT_IOCHARSET="utf8"

CONFIG_NTFS3_FS=y
CONFIG_NTFS3_LZX_XPRESS=y

# Samba (SMB) support
CONFIG_CIFS=y
CONFIG_SMBFS=y
CONFIG_CIFS_XATTR=y
CONFIG_CIFS_POSIX=y

# NFS 4.1/4.2 support (3 and 4.0 is on per default)
CONFIG_NFS_V4_1=y
CONFIG_NFS_V4_2=y

# Enable Transparent hugepages for programs that request it (MADVISE)
CONFIG_ARCH_ENABLE_THP_MIGRATION=y
CONFIG_TRANSPARENT_HUGEPAGE=y
CONFIG_TRANSPARENT_HUGEPAGE_MADVISE=y
CONFIG_THP_SWAP=y

# Enable new MGLRU memory reclamation
# Help a lot with performance in low memory situations
CONFIG_LRU_GEN=y
CONFIG_LRU_GEN_ENABLED=y

# Gamepad support (Steam, Xbox, Playstation controllers)
CONFIG_INPUT_JOYDEV=y
CONFIG_INPUT_UINPUT=y

CONFIG_JOYSTICK_XPAD=y
CONFIG_JOYSTICK_XPAD_FF=y
CONFIG_JOYSTICK_XPAD_LEDS=y

CONFIG_LEDS_CLASS_MULTICOLOR=y
CONFIG_HID_PLAYSTATION=y
CONFIG_PLAYSTATION_FF=y
CONFIG_SONY_FF=y

CONFIG_HID_STEAM=y
CONFIG_HID_BATTERY_STRENGTH=y

# Bluetooth kb/m support
CONFIG_UHID=y

# Wacom drawing tablet support
CONFIG_HID_WACOM=y
# Huion drawing tablet support
CONFIG_HID_UCLOGIC=y

# Sound driver support
CONFIG_SND_RAWMIDI=y
CONFIG_SND_DYNAMIC_MINORS=y
CONFIG_SND_MAX_CARDS=32
CONFIG_SND_CTL_LED=y
CONFIG_SND_SEQ_MIDI_EVENT=y
CONFIG_SND_SEQ_MIDI=y
CONFIG_SND_HDA_GENERIC_LEDS=y
CONFIG_SND_HDA_RECONFIG=y
CONFIG_SND_HDA_PATCH_LOADER=y
CONFIG_SND_HDA_CODEC_REALTEK=y
CONFIG_SND_HDA_CODEC_HDMI=y
CONFIG_SND_HDA_GENERIC=y
CONFIG_SND_USB_AUDIO=y
CONFIG_SND_USB_AUDIO_USE_MEDIA_CONTROLLER=y
CONFIG_LEDS_TRIGGER_AUDIO=y

# Webcam support
CONFIG_MEDIA_SUPPORT=y
CONFIG_MEDIA_SUPPORT_FILTER=y
CONFIG_MEDIA_SUBDRV_AUTOSELECT=y
CONFIG_MEDIA_CAMERA_SUPPORT=y
CONFIG_VIDEO_DEV=y
CONFIG_MEDIA_CONTROLLER=y
CONFIG_VIDEO_V4L2_I2C=y
CONFIG_VIDEO_V4L2_SUBDEV_API=y
CONFIG_MEDIA_USB_SUPPORT=y
CONFIG_USB_GSPCA=m
CONFIG_USB_VIDEO_CLASS=y
CONFIG_USB_VIDEO_CLASS_INPUT_EVDEV=y

CONFIG_VIDEOBUF2_CORE=y
CONFIG_VIDEOBUF2_V4L2=y
CONFIG_VIDEOBUF2_MEMOPS=y
CONFIG_VIDEOBUF2_VMALLOC=y

CONFIG_MEDIA_HIDE_ANCILLARY_SUBDRV=y

# Bluetooth support
CONFIG_BT=m
CONFIG_BT_BREDR=y
CONFIG_BT_RFCOMM=y
CONFIG_BT_HIDP=y
CONFIG_BT_HS=y
CONFIG_BT_LE=y
CONFIG_BT_LE_L2CAP_ECRED=y
CONFIG_BT_LEDS=y
CONFIG_BT_DEBUGFS=y

CONFIG_BT_INTEL=m
CONFIG_BT_BCM=m
CONFIG_BT_RTL=m
CONFIG_BT_HCIBTUSB=m
CONFIG_BT_HCIBTUSB_POLL_SYNC=y
CONFIG_BT_HCIBTUSB_BCM=y
CONFIG_BT_HCIBTUSB_MTK=y
CONFIG_BT_HCIBTUSB_RTL=y
CONFIG_BT_HCIUART=m

CONFIG_CRYPTO_ECC=m
CONFIG_CRYPTO_ECDH=m
CONFIG_CRYPTO_ECB=m

# Turn on frame buffer console
CONFIG_SYSFB_SIMPLEFB=y
CONFIG_DRM_SIMPLEDRM=y
CONFIG_DRM_FBDEV_EMULATION=y

CONFIG_FRAMEBUFFER_CONSOLE=y
CONFIG_FRAMEBUFFER_CONSOLE_DETECT_PRIMARY=y

# AMDGPU support
CONFIG_DRM_AMDGPU=m
CONFIG_DRM_AMDGPU_USERPTR=y

CONFIG_DRM_AMD_DC=y
CONFIG_DRM_AMD_DC_DCN=y
CONFIG_DRM_AMD_DC_HDCP=y

CONFIG_HSA_AMD=y

# For the closed source NVIDIA driver
CONFIG_X86_KERNEL_IBT=n

# USB4/Thunderbolt support
CONFIG_HOTPLUG_PCI_PCIE=y
CONFIG_HOTPLUG_PCI_ACPI=y
CONFIG_USB4=y
CONFIG_HOTPLUG_PCI_SHPC=y
CONFIG_TYPEC=y
CONFIG_TYPEC_TCPM=y
CONFIG_TYPEC_TCPCI=y
CONFIG_TYPEC_UCSI=y
CONFIG_UCSI_ACPI=y
CONFIG_TYPEC_DP_ALTMODE=y
CONFIG_TYPEC_NVIDIA_ALTMODE=y

# Needed for efibootmgr
CONFIG_EFIVAR_FS=y
" > /tmp/extra_kernel_config

# KVM/Virtualization options
echo "
CONFIG_HAVE_KVM_PFNCACHE=y
CONFIG_HAVE_KVM_IRQCHIP=y
CONFIG_HAVE_KVM_IRQFD=y
CONFIG_HAVE_KVM_IRQ_ROUTING=y
CONFIG_HAVE_KVM_DIRTY_RING=y
CONFIG_HAVE_KVM_EVENTFD=y
CONFIG_KVM_MMIO=y
CONFIG_KVM_ASYNC_PF=y
CONFIG_HAVE_KVM_MSI=y
CONFIG_HAVE_KVM_CPU_RELAX_INTERCEPT=y
CONFIG_KVM_VFIO=y
CONFIG_KVM_GENERIC_DIRTYLOG_READ_PROTECT=y
CONFIG_KVM_COMPAT=y
CONFIG_HAVE_KVM_IRQ_BYPASS=y
CONFIG_HAVE_KVM_NO_POLL=y
CONFIG_KVM_XFER_TO_GUEST_WORK=y
CONFIG_HAVE_KVM_PM_NOTIFIER=y
CONFIG_KVM=y
CONFIG_KVM_INTEL=m
CONFIG_KVM_AMD=m

CONFIG_VHOST_NET=y
CONFIG_TUN=y
CONFIG_BRIDGE=y
CONFIG_IP_NF_NAT=y
CONFIG_IP_NF_TARGET_MASQUERADE=y

CONFIG_VFIO=m
CONFIG_VFIO_IOMMU_TYPE1=m
CONFIG_VFIO_VIRQFD=m
CONFIG_VFIO_NOIOMMU=y
CONFIG_VFIO_PCI_CORE=m
CONFIG_VFIO_PCI_MMAP=y
CONFIG_VFIO_PCI_INTX=y
CONFIG_VFIO_PCI=m
CONFIG_VFIO_PCI_VGA=y
CONFIG_VFIO_PCI_IGD=y
CONFIG_IRQ_BYPASS_MANAGER=y

CONFIG_IOMMU_DEFAULT_PASSTHROUGH=y
CONFIG_IOMMU_SVA=y
CONFIG_AMD_IOMMU_V2=y
CONFIG_INTEL_IOMMU_SVM=y
CONFIG_INTEL_IOMMU_DEFAULT_ON=y
CONFIG_IRQ_REMAP=y
" >> /tmp/extra_kernel_config

# NFS server support (for hosting NFS network shares)
#echo "
#CONFIG_NFSD=y
#CONFIG_NFSD_V3_ACL=y
#CONFIG_NFSD_V4=y
#" >> /tmp/extra_kernel_config

cd /usr/src/linux/

# Inintialize a default kernel config
make olddefconfig

# Try to create a kernel config with only the locally loaded modules
make localmodconfig &> /tmp/localmod_output
# If a config for a module is commented out, it will print "module <module> did not have configs <config option>"
# Use this output to active these modules.
grep -oP "did not have configs \K.*" /tmp/localmod_output | sed -e 's/$/=m/' >> .config

# Add out config changes ontop of the default config
cat /tmp/extra_kernel_config >> .config

# Build all network card modules (in the network section of the config)
#sed '1,/CONFIG_ETHERNET=y/d' .config | # Filter out all lines before CONFIG_ETHERNET ...
#sed '/CONFIG_NET_VENDOR_XIRCOM=y/,$d' | # and everything after CONFIG_NET_VENDOR_XIRCOM
#sed -n '/is not set/p' | # Filter out all non "is not set lines"
#sed 's/ is not set/=m/' | sed 's/# //' > /tmp/network_card_modules # Convert all line with "is not set" into modules.
#
#cat /tmp/network_card_modules >> .config

# Apply our kernel config changes
make olddefconfig

# Compile the kernel and install the modules
make -j$(nproc) && make modules_install

# Make sure that /boot is mounted (if we are running this manually outside of the installer)
mountpoint -q /boot || mount /boot

# Install the boot image to /boot
make install
