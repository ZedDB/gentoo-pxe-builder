Files in this folder:

# skel
Misc /etc/skel default user settings

# skel_bashrc_extras
What to append to the /etc/skel/.bashrc file on the Server/Client

# en_NL
English with dutch date format and misc conventions for glibcEnglish with dutch date format and misc conventions for glibc

# configure_kernel.sh
Kernel configure and install script

# ipxe.efi
IPXE boot playload to be installed on the Server/Client

# autoexec.ipxe
IPXE boot playload autostart configuration

# chrootstart.sh
The install script to run once we have chrooted

# install_env_vars
File containing the env vars to use for the user credentials and timezone settings

# repair_efi_entries.sh
Script to try to repair the EFI boot entries if they have been wiped
