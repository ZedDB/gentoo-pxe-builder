gentoo-pxe-builder
==================

The code based on and inspired by:
- https://github.com/NiKiZe/gentoo-pxe-builder
- https://github.com/NiKiZe/Gentoo-HAI

## Purpose
The purpose of this PXE builder is to lets you create a bootable PXE image that can automatically setup computers with minimal input.
You can also access it the computer remotely using SSH.

This PXE image can then either be hosted on a http server or booted from a USB stick. We will use iPXE to handle the network and USB boot process.

## What it does for you
This automation script prepares a Gentoo kernel and initramfs suitable to boot from PXE:
* It bases itself on the latest minimal ISO available
* It will **start an install script and start the SSH daemon**

It lets you take control of the PXE host easily:
* **You can change the files in `files/updates`** and adjust it to your needs and rebuild the whole PXE stack by simply re-runing the build.sh script
* Your SSH public key will already be present on the machine so you can connect without password to the booted machine

## What will happen when I boot ?
* At first this will start your machine just like the regular livecd
* It will automatically start dhcpcd and the SSH daemon
* Then it will show if any of your network adapters has been configured for WoL (Wake on Lan)
* Finally, it will start the bundled install script (`files/updates/root/install.sh`) and wait for user input.
* If you want to run the installer over SSH you can execute it after logging in with `./install.sh`. 

## Usage
If you just want to get a standard install going, you should only have to run the `build.sh` script to build the PXE image.
After that is done you probably want to setup iPXE USB boot drives and optionally an http server.
Read the readme files in `ipxe_builder` and `http` respectively. 

If you wish to customize any of the install steps I recommend to read through the installation section of the Gentoo handbook while you are following along the `install.sh` and `chrootstart.sh` scripts.
After you have made your modifications, just execute the `build.sh` script and enjoy.

## Prerequisites

* The `cpio` command from `app-arch/cpio`
* The `gpg` command from `app-crypt/gnupg`
* The `xz` command from `app-arch/xz-utils`
* The `isoinfo` command from `app-cdr/cdrtools`

## Environment variables and defaults
* ARCH : `amd64`
* MIRROR : `http://distfiles.gentoo.org`
* SSH_KEY_PATH : `~/.ssh/id_rsa.pub`
